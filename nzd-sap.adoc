= Near Zero Downtime Maintenance for SAP
Ricardo Garcia Cavero @rgarciac
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._

*Use case:* Minimizing the downtime of the maintenance on SAP hosts so that users and processes can continue to work
without perceiving any interruption.

*Background:* SAP workloads are critical to a company and the maintenance windows are often very strict, sometimes making it difficult for the system
administrators to finish updates and maintenance tasks properly in time.

== Solution overview
This solution allows for application of OS patches, fixes,
and system updates, as well as database patching and version updates, and SAP kernel updates. With this portfolio architecture implemented users can continue to work inside SAP, without noticing
any disruption.

====
*Near Zero Downtime Maintenance for SAP*

. Full lifecycle management of all SAP hosts 
. Keep the SAP landscape compliant and consistent
. Avoid disruptions during maintenance
====



--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/near-zero-downtime-maintenance-for-sap-marketing.png[alt="Architecture based on HA to achieve Near Zero Downtime in maintenance of RHEL servers running SAP workloads", width=700]
--
== Summary video
video::87lISBZZc6w[youtube]


== Logical diagram
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/nzd-sap.png[alt="RHEL servers hosting SAP workloads, Red Hat Satellite and Red Hat Ansible Automation Platform on premise, private, public or hybrid cloud and ", width=700]
--

== The technology
The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux for SAP Solutions*] is combining an intelligent operating system with predictive management
tools and SAP-specific content, Red Hat Enterprise Linux for SAP Solutions provides a single, consistent, highly
available foundation for business-critical SAP and non-SAP workloads.

https://www.redhat.com/en/technologies/management/satellite?intcmp=7013a00000318EWAAY[*Red Hat Satellite*] the smart management element in this architecture, used for tracking, managing, auditing, and
collecting data on the entire infrastructure to ensure that baselines are met.

https://www.redhat.com/en/technologies/management/smart-management?intcmp=7013a00000318EWAAY[*Red Hat Smart Management*], which includes Satellite and Cloud Connector, provides the capability to gather
anonymized configuration information from the SAP hosts and send that anonymized data to Insights Platform (on Red
Hat’s SaaS). Satellite manages the lifecycle of the SAP servers, applying the packages, security fixes, etc., that
they need to be compliant with SAP’s and Red Hat’s recommendations and consistent between them.

https://www.redhat.com/en/technologies/management/ansible?intcmp=7013a00000318EWAAY[*Red Hat Ansible Automation Platform*] is the framework used in this solution to run the remediation Ansible
playbooks in the hosts that will correct the situations that could lead to a failure or issue, for example modifying
a kernel memory parameter that can cause a bad performance of the SAP HANA DB or applying a certain level of an OS
package that is needed for a particular version of SAP Netweaver.
====

== Architectures
=== Near zero downtime network overview
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/nzd-sap-network-sd.png[alt="Network connections from Red Hat Satellite and Red Hat Ansible Automation Platform servers to RHEL servers hosting SAP HANA and SAP S/4HANA or SAP Netweaver", width=700]
--
SAP HANA is the only DB for which this solution has been implemented by customers. However it could potentially be
implemented for other DBs supported by SAP like DB2 using its HADR capability and triggering the fail-over of the
resources with Ansible playbooks just as it is done in this implementation with SAP HANA.

On the application side we can have any SAP Netweaver based application (either the new suite built on SAP S/4HANA or
legacy systems based on SAP Netweaver like SAP Netweaver itself, SAP BW, SAP PO/PI, etc.). The application hosts are
connected with the SAP HANA DB hosts and all these servers that host SAP workloads are connected with those belonging
to the Infrastructure Management tier, to both the Automation Orchestration (or Ansible Automation Platform) and to
Satellite.

=== Near zero downtime data overview
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/nzd-sap-data-sd.png[alt="Workflow for Near Zero Downtown Maintenance on SAP HANA and SAP S/4HANA or SAP Netweaver RHEL servers orchestrated by Red Hat Ansible Automation Platform and using Red Hat Satellite and Red Hat HA Add-On to operate Red Hat Pacemaker clusters", width=700]
--

All the SAP hosts are sending status data to Satellite that is in charge of their lifecycle management. The DB tier,
the application tier or both are clustered. So we have Pacemaker cluster of the SAP HANA DB (the deployment of the DB
can be scale-up - with just two servers with the exact same instance being replicated in real-time - or scale-out -
with the different services of the DB spread across multiple nodes in order to have larger resources - both models can
be clustered) and/or Pacemaker cluster of the application (SAP S/4HANA or any other SAP Netweaver based one).The RHEL HA
Add-On based on Pacemaker has specific resources for SAP HANA and also for the application tier.

The flow represented in this schematic diagram is the following:

The Satellite server applies the packages, security fixes, etc., in the primary node of the target cluster (DB
cluster or application cluster - SAP Netweaver or SAP S/4HANA). If the intervention is a SAP HANA upgrade (DB cluster)
or a SAP kernel upgrade (application cluster - the SAP kernel is different from the OS kernel, it is another layer of
binaries specific to the SAP application) it will be the Automation Orchestration (Ansible Tower) who will run a
playbook for this upgrade in the primary node of the target cluster.

If the maintenance is in the SAP HANA hosts, once the intervention is finished in the primary node, the Automation
Orchestration runs a playbook in any of the nodes of the cluster to move the virtual IP to the other node so that the
application servers can connect to it and keep working and since the SAP application ‘suspend DB connection’ feature
is used, no transactions will be committed to the DB until the virtual IP failover is done, this takes less than one
second so users will not perceive any disconnection. The Automation Orchestration will also run another playbook on
any of the cluster nodes to change the direction of the SAP HANA System Replication, so that the node where the
maintenance has already been done becomes the primary of this replication (primary node). If the maintenance is in the
SAP application (Netweaver or S/4HANA) cluster the cluster resources that will be moved while the primary node is
under maintenance will be the SAP instance (ASCS or ERS, depending on the one that is in the node) and the filesystems
with the work and profile directories of the instance.

The Satellite server performs the intervention in the former primary node of the cluster. As in step 1, if the
intervention is a SAP HANA upgrade or a SAP kernel upgrade it will be the Automation Orchestration who will run a
playbook for it to be done in the former primary node of the cluster.

After the intervention has been finished we can revert to the initial configuration of primary and secondary nodes 
of the cluster or keep the current one.

== Download solution diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/nzd-sap.drawio[[Open Diagrams]]
--

== Provide feedback 
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/nzd-sap.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
